@echo off

rem Remove deprecated iserv-lock
sc delete iserv-lock >nul

rem Install service iserv-lock2

rem Windows 2000 and XP
ver | find "Version 5." >nul
if errorlevel 1 goto fi
  \\iserv\netlogon\exe\iserv-lock.exe -i
  exit
:fi

rem Windows Vista and 7
ver | find "Version 6." >nul
if errorlevel 1 goto fi
  \\iserv\netlogon\exe\WindowsServiceIservLock.exe --install >nul
  exit
:fi

echo "Windows version not supported!"

