@echo off
rem Logoff current user from workstation with countdown.
rem Usage: logoff.bat [timeout]

rem Bug: Cannot check, if there is acutally a user logged in.

rem Parse arguments
set T=30
if not "%1"=="" set T=%1

rem Trigger reboot in T seconds for countdown
shutdown -r -t %T%

rem Wait T - 3 seconds
set /a P=%T%-2
ping -n %P% 127.0.0.1 >nul

rem Abort reboot
shutdown -a | find "."

rem Exit if countdown was aborted
if not errorlevel 1 goto exit

rem Wait 3 seconds
ping -n 3 127.0.0.1 >nul

rem Logoff user
echo y | rwinsta 0

:exit
