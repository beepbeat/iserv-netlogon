﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using WindowsServiceIservLock.Properties;

namespace WindowsServiceIservLock
{
    public partial class IservLockService : ServiceBase
    {
        public IservLockService()
        {
            InitializeComponent();
        }

        private Process lockProcess;

        protected override void OnStart(string[] args)
        {
            start();
        }

        private void start()
        {
            lockProcess = InteractiveProcess.Start(
                Settings.Default.LockApplication,
                Settings.Default.LockApplicationArguments,
                Settings.Default.ExistingUserProcess
                );

            lockProcess.EnableRaisingEvents = true;

            /*
               The following line prevents users from circumventing the lock
               screen like this:

               1) Before the screen is locked, open up notepad and type
                  something. Don't save the file.
               2) After the screen has been locked, press CTRL+ALT+DEL, and
                  choose Log off.
               3) Windows will kill all open applications (including
                  iserv-lock.exe), but will fail to kill notepad because it'll
                  ask whether you want to save the file.
               4) Choose Cancel in all dialogs.
               5) You have now circumvented the lock screen.

               The credit for discovering this bug goes to Karsten Wulf and his
               students:
               https://support.iserv.eu/idesk/msg/msg.php?forums/public/Systemfragen/9752

               We prevent this by just restarting iserv-lock.exe whenever it
               exits. Only stopping the service (which requires administrator
               privileges) will now disable the lock screen.
            */
            lockProcess.Exited += (sender, eventArgs) => start();
        }

        protected override void OnStop()
        {
            try
            {
                if (lockProcess != null)
                    if (!lockProcess.HasExited)
                        lockProcess.Kill();
            }
            catch (Win32Exception ex)
            {
                EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
