﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace WindowsServiceIservLock
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            string parameter = string.Concat(args);
            switch (parameter)
            {
                case "--install":
                    ManagedInstallerClass.InstallHelper(new string[] {
			"/LogFile=" + Path.GetTempPath() +  "\\IservLockService.install.log",
			"/InstallStateDir=" + Path.GetTempPath(),
			Assembly.GetExecutingAssembly().Location });
                    break;
                case "--uninstall":
                    ManagedInstallerClass.InstallHelper(new string[] {
			"/LogFile=" + Path.GetTempPath() + "\\IservLockService.uninstall.log",
			"/InstallStateDir=" + Path.GetTempPath(),
			"/u",
			Assembly.GetExecutingAssembly().Location });
                    break;
                default:
		    if (Environment.UserInteractive)
			Console.WriteLine("supported parameters are --install or --uninstall");
		    else
			ServiceBase.Run(new ServiceBase[] { new IservLockService() });
		    break;
	    }
        }
    }
}
