#include <windows.h>

VOID die(LPCTSTR szMsg)
{
  LPTSTR lpBuffer = NULL;
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
      NULL, GetLastError(), 0, (LPTSTR)&lpBuffer, 0, NULL);
  MessageBox(NULL, lpBuffer, szMsg, MB_ICONSTOP | MB_OK);
  LocalFree(lpBuffer);
  _exit(-1);
}

INT WINAPI WinMain(HINSTANCE hInstanceL, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
  PROCESS_INFORMATION p;
  STARTUPINFO s;
  ZeroMemory(&s, sizeof(s));
  s.cb = sizeof(s);
  if (!CreateProcess(NULL, lpCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &s, &p))
    die("CreateProcess");
  return 0;
}

