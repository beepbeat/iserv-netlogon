#include <windows.h>

VOID die(LPCTSTR szMsg)
{
  LPTSTR lpBuffer = NULL;
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
      NULL, GetLastError(), 0, (LPTSTR)&lpBuffer, 0, NULL);
  MessageBox(NULL, lpBuffer, szMsg, MB_ICONSTOP | MB_OK);
  LocalFree(lpBuffer);
  _exit(-1);
}

INT WINAPI WinMain(HINSTANCE hInstanceL, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
  char szBuffer[0x10000];
  strncpy(szBuffer, lpCmdLine, sizeof(szBuffer));

  PCHAR arg = strtok(lpCmdLine, " ");
  if (!strcmp(arg, "-f"))
  {
    arg = strtok(NULL, "");
    if (arg)
    {
      HANDLE hFile = CreateFile(arg,
	GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
      if (hFile == INVALID_HANDLE_VALUE)
	die("CreateFile");

      DWORD dwFileSize = GetFileSize(hFile, NULL);
      if (dwFileSize == 0xffffffff)
	die("GetFileSize");

      DWORD dwRead;
      if (!ReadFile(hFile, szBuffer, sizeof(szBuffer), &dwRead, NULL))
	die("ReadFile");
      szBuffer[dwRead] = '\0';

      if (!CloseHandle(hFile))
	die("CloseHandle");
    }
  }
  else if (!strcmp(arg, "-h"))
  {
    MessageBox(NULL,
	"-f file\tshow file content\n"
	"-h\tshow help\n",
	"Usage", MB_OK);
    _exit(-1);
  }

  if (!MessageBox(NULL, *szBuffer? szBuffer: lpCmdLine, "IServ",
	MB_SERVICE_NOTIFICATION))
    die("MessageBox");

  return 0;
}

