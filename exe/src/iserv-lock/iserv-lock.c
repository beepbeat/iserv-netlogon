#include <windows.h>

LPCTSTR AppName = "iserv-lock2";
LPCTSTR DisplayName = "IServ Client Lock";
LPCTSTR Image = "iserv-lock.bmp";
LPCTSTR MsgText = "Diese Arbeitsstation wurde gesperrt.";
int MsgSize = 39;
int MsgColor = 0xdcdcdc; // grey 220

HHOOK KeyboardHook;
HHOOK MouseHook;
HINSTANCE hInstance;
HWND hWnd;
HBITMAP hBitmap;
BOOL debug = FALSE;
BOOL stop = FALSE;

BOOL opt_install = FALSE;
BOOL opt_uninstall = FALSE;
BOOL opt_silent = FALSE;

VOID die(LPCTSTR szMsg)
{
  if (!opt_silent)
  {
    LPTSTR lpBuffer = NULL;
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
	NULL, GetLastError(), 0, (LPTSTR)&lpBuffer, 0, NULL);
    MessageBox(NULL, lpBuffer, szMsg, MB_ICONSTOP | MB_OK);
    LocalFree(lpBuffer);
  }
  _exit(-1);
}

VOID paint(HWND hWnd)
{
  RECT w;
  BITMAP b = {0, 0, 0, 0};
  HDC hDC, hDCB;
  PAINTSTRUCT Ps;
  HFONT font;

  GetWindowRect(hWnd, &w);
  hDC = BeginPaint(hWnd, &Ps);

  // Draw Image
  if (!hBitmap)
    hBitmap = (HBITMAP)LoadImage(NULL, Image, IMAGE_BITMAP, 0, 0,
	LR_LOADFROMFILE | LR_CREATEDIBSECTION | LR_DEFAULTSIZE);

  if (hBitmap)
  {
    hDCB = CreateCompatibleDC(hDC);
    SelectObject(hDCB, hBitmap);
    GetObject(hBitmap, sizeof(BITMAP), &b);
    BitBlt(hDC,
	(w.right - w.left - b.bmWidth) / 2,
	(w.bottom - w.top - b.bmHeight) / 2 - MsgSize,
	b.bmWidth, b.bmHeight, hDCB, 0, 0, SRCCOPY);
    DeleteDC(hDCB);
  }

  // Draw Text
  SetTextColor(hDC, MsgColor);
  SetBkColor(hDC, TRANSPARENT);
  font = CreateFont(MsgSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE,
      ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
      DEFAULT_PITCH | FF_ROMAN, "Verdana");
  SelectObject(hDC, font);
  SetTextAlign(hDC, TA_TOP | TA_CENTER);
  TextOut(hDC,
      (w.right - w.left) / 2,
      (w.bottom - w.top + b.bmHeight) / 2,
      MsgText, strlen(MsgText));
  DeleteObject(font);

  EndPaint(hWnd, &Ps);
}

LRESULT CALLBACK LowLevelKeyboardProc(INT nCode, WPARAM wParam, LPARAM lParam)
{
  if (!debug && (nCode == HC_ACTION))
    return 1;
  CallNextHookEx(KeyboardHook, nCode, wParam, lParam);
  return 0;
}

LRESULT CALLBACK LowLevelMouseProc(INT nCode, WPARAM wParam, LPARAM lParam)
{
  if (!debug)
    return 1;
  CallNextHookEx(MouseHook, nCode, wParam, lParam);
  return 0;
}

LRESULT CALLBACK WndProcedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
  if (!stop && (Msg == WM_CLOSE))
    return 1;
  if (Msg == WM_DESTROY)
    PostQuitMessage(WM_QUIT);
  else if (Msg == WM_PAINT)
    paint(hWnd);
  return DefWindowProc(hWnd, Msg, wParam, lParam);
}

int lock()
{
  WNDCLASS WndCls;
  WndCls.style = CS_HREDRAW | CS_VREDRAW;
  WndCls.lpfnWndProc = WndProcedure;
  WndCls.cbClsExtra = 0;
  WndCls.cbWndExtra = 0;
  WndCls.hInstance = hInstance;
  WndCls.hIcon = 0;
  WndCls.hCursor = LoadCursor(NULL, IDC_ARROW);
  WndCls.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  WndCls.lpszMenuName = NULL;
  WndCls.lpszClassName = AppName;
  RegisterClass(&WndCls);

  hWnd = CreateWindow(
      AppName,
      DisplayName,
      debug?
	(WS_VISIBLE | WS_OVERLAPPEDWINDOW):
        (WS_VISIBLE | WS_POPUP | WS_MAXIMIZE),
      CW_USEDEFAULT,
      CW_USEDEFAULT,
      CW_USEDEFAULT,
      CW_USEDEFAULT,
      NULL,
      NULL,
      hInstance,
      NULL);
  if (!hWnd)
    die("Cannot create window");

  if (!debug)
  {
    SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0,
	SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
    ShowCursor(FALSE);
  }

  KeyboardHook = SetWindowsHookEx(
      WH_KEYBOARD_LL, LowLevelKeyboardProc, hInstance, 0);
  if (!KeyboardHook)
    die("Error hooking keyboard");

  MouseHook = SetWindowsHookEx(
      WH_MOUSE_LL, LowLevelMouseProc, hInstance, 0);
  if (!MouseHook)
    die("Error hooking mouse");

  MSG Msg;
  while (GetMessage(&Msg, NULL, 0, 0))
  {
    TranslateMessage(&Msg);
    DispatchMessage(&Msg);
  }
  return Msg.wParam;
}

VOID WINAPI ServiceCtrlHandler(DWORD dwControl)
{
  if (dwControl == SERVICE_CONTROL_STOP)
  {
    stop = TRUE;
    SendMessage(hWnd, WM_CLOSE, 0, 0);
  }
}

VOID WINAPI ServiceMain(DWORD argc, LPTSTR *argv)
{
  SERVICE_STATUS s;
  SERVICE_STATUS_HANDLE h;

  h = RegisterServiceCtrlHandler(AppName, ServiceCtrlHandler);
  if (!h)
    return;

  s.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
  s.dwCurrentState = SERVICE_RUNNING;
  s.dwControlsAccepted = SERVICE_ACCEPT_STOP;
  s.dwWin32ExitCode = 0;
  s.dwServiceSpecificExitCode = 0;
  s.dwCheckPoint = 0;
  s.dwWaitHint = 0;
  if (!SetServiceStatus(h, &s))
    die("SetServiceStatus");

  lock();

  s.dwCurrentState = SERVICE_STOPPED;
  if (!SetServiceStatus(h, &s))
    die("SetServiceStatus");
}

INT WINAPI WinMain(HINSTANCE hInstanceL, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
  PCHAR arg = strtok(lpCmdLine, " ");
  while (arg)
  {
    if (!strcmp(arg, "-i"))
      opt_install = TRUE;
    else if (!strcmp(arg, "-u"))
      opt_uninstall = TRUE;
    else if (!strcmp(arg, "-s"))
      opt_silent = TRUE;
    else
    {
      MessageBox(NULL,
	  "-i\tinstall service\n"
	  "-u\tuninstall service\n"
	  "-s\tsilent\n",
	  "Usage", MB_OK);
      _exit(-1);
    }
    arg = strtok(NULL, " ");
  }

  if (opt_install || opt_uninstall)
  {
    SC_HANDLE scmHandle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!scmHandle)
      die("OpenSCManager");
    if (opt_install)
    {
      char szPath[MAX_PATH];
      GetModuleFileName(GetModuleHandle(NULL), szPath, MAX_PATH);

      char szBuffer[MAX_PATH + 2];
      strcpy(szBuffer, "\"");
      strcat(szBuffer, szPath);
      strcat(szBuffer, "\"");

      SC_HANDLE scHandle = CreateService(
	  scmHandle,
	  AppName,
	  DisplayName,
	  SERVICE_ALL_ACCESS,
	  SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
	  SERVICE_DEMAND_START,
	  SERVICE_ERROR_NORMAL,
	  szBuffer,
	  NULL,
	  NULL,
	  NULL,
	  NULL,
	  NULL);
      if (!scHandle)
	die("CreateService");

      if (!CloseServiceHandle(scHandle))
	die("CloseServiceHandle");
    }
    else
    {
      SC_HANDLE scHandle = OpenService(scmHandle, AppName, SERVICE_ALL_ACCESS);
      if (!scHandle)
	die("OpenService");
      if (!DeleteService(scHandle))
	die("DeleteService");
      if (!CloseServiceHandle(scHandle))
	die("CloseServiceHandle");
    }
    CloseServiceHandle(scmHandle);
  }
  else
  {
    SERVICE_TABLE_ENTRY DispatchTable[] =
	{ { (LPTSTR) AppName, ServiceMain }, { 0, 0 } };
    hInstance = hInstanceL;

    if (!StartServiceCtrlDispatcher(DispatchTable))
      lock();
  }
  return 0;
}

