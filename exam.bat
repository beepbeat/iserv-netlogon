@echo off

rem Some programs modify the PATH; we need to reset it to the default to
rem ensure that we call the right programs, e.g. MS find and not GNU find.
rem This only affects the login script environment, not the whole system.
set path=%systemroot%\system32;%systemroot%;%systemroot%\system32\wbem;\\iserv\netlogon\exe

ver | find "Version 5.0" >nul
if %ERRORLEVEL% == 0 set IPSEC=ipsecpol.exe
ver | find "Version 5.1" >nul
if %ERRORLEVEL% == 0 set IPSEC=ipseccmd.exe
ver | find "Version 5.2" >nul
if %ERRORLEVEL% == 0 set IPSEC=ipseccmd.exe
ver | find "Version 6.1" >nul
if %ERRORLEVEL% == 0 set IPSEC=win7

if "%IPSEC%" == "" (
  echo Operating system not supported.
  goto exit
)

if "%IPSEC%" == "win7" (
  rem Windows 7

  if /I "%1"=="start" (
    rem Delete legacy rules if they exist
    netsh ipsec static delete policy name="IServ Exam Mode" >nul
    netsh ipsec static delete filterlist name="IServ Exam Mode Block List" >nul
    netsh ipsec static delete filteraction name="IServ Exam Mode Block Action" >nul
    netsh ipsec static delete filterlist name="IServ Exam Mode Allow List" >nul
    netsh ipsec static delete filteraction name="IServ Exam Mode Allow Action" >nul

    rem Backup and reset firewall
    if exist %systemdrive%\exam.wfw del /f %systemdrive%\exam.wfw
    netsh advfirewall export %systemdrive%\exam.wfw >nul
    netsh advfirewall reset >nul
    rem Allow IServ
    netsh advfirewall firewall add rule name="IServ Exam - Allow IServ" dir=out action=allow remoteip=wins >nul
    netsh advfirewall firewall add rule name="IServ Exam - Allow IServ" dir=in action=allow remoteip=wins >nul
    rem Allow NBNS (NetBIOS Name Service)
    netsh advfirewall firewall add rule name="IServ Exam - Allow NBNS" dir=out action=allow remoteip=localsubnet protocol=udp remoteport=137 >nul
    netsh advfirewall firewall add rule name="IServ Exam - Allow NBNS" dir=in action=allow remoteip=localsubnet protocol=udp remoteport=137 >nul
    rem Add your own whitelisted servers here:
    rem --------------------------------------
    rem netsh advfirewall firewall add rule name="IServ Exam - Allow my host" dir=out action=allow remoteip=10.x.y.z >nul
    rem netsh advfirewall firewall add rule name="IServ Exam - Allow my host" dir=in action=allow remoteip=10.x.y.z >nul
    rem --------------------------------------
    rem Block everything
    netsh advfirewall set allprofiles firewallpolicy blockinbound,blockoutbound >nul
  )
  if /I "%1"=="stop" (
    rem Delete legacy rules if they exist
    netsh ipsec static delete policy name="IServ Exam Mode" >nul
    netsh ipsec static delete filterlist name="IServ Exam Mode Block List" >nul
    netsh ipsec static delete filteraction name="IServ Exam Mode Block Action" >nul
    netsh ipsec static delete filterlist name="IServ Exam Mode Allow List" >nul
    netsh ipsec static delete filteraction name="IServ Exam Mode Allow Action" >nul

    netsh advfirewall reset >nul
    if exist %systemdrive%\exam.wfw (
      rem Restore old firewall settings
      netsh advfirewall import %systemdrive%\exam.wfw >nul
      del /f %systemdrive%\exam.wfw
    ) else (
      rem There is no config backup; disabling the firewall seems the most
      rem sensible thing to do because that's our default in opsi anyway.
      netsh advfirewall set allprofiles state off >nul
    )
  )
) else (
  rem Windows 2000/XP/2003

  if /I "%1"=="start" (
    %IPSEC% -w REG -p "IServ Exam Mode" -o >nul 2>nul
    %IPSEC% -w REG -p "IServ Exam Mode" -r AllowIServ -f 0+iserv -n PASS >nul
    rem Add your own whitelisted servers here:
    rem --------------------------------------
    rem %IPSEC% -w REG -p "IServ Exam Mode" -r AllowMyRule1 -f 0+10.1.2.3 -n PASS >nul
    rem --------------------------------------
    %IPSEC% -w REG -p "IServ Exam Mode" -r BlockEverything -f *+* -n BLOCK >nul
    %IPSEC% -w REG -p "IServ Exam Mode" -x >nul
  )
  if /I "%1"=="stop" (
    %IPSEC% -w REG -p "IServ Exam Mode" -y >nul 2>nul
  )
)

:exit

rem call exam-local.bat if it exists
if exist \\iserv\netlogon\exam-local.bat call \\iserv\netlogon\exam-local.bat

