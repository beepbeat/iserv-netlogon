@echo off
rem Windows Login Script for IServ
rem
rem This file is generated automatically by iservchk.
rem It is not recommended to make any changes to this file.
rem If really necessary you can save changes permanently using:
rem   iconf save /etc/samba/netlogon/login.bat
rem
rem For your own settings you can use:
rem   /etc/samba/netlogon/local.bat

rem Some programs modify the PATH; we need to reset it to the default to
rem ensure that we call the right programs, e.g. MS find and not GNU find.
rem This only affects the login script environment, not the whole system.
set path=%systemroot%\system32;%systemroot%;%systemroot%\system32\wbem

if /i "%userdomain%"=="schule" goto success
echo Computer (%userdomain%) is not member of IServ domain (schule). Aborting.
pause
goto :eof
:success

rem Import registry keys
rem All Windows versions
reg import \\iserv\netlogon\login.reg
reg import \\iserv\netlogon\local.reg
rem Windows 2000 and XP
ver | find "Version 5." > nul
if errorlevel 1 goto fi
  reg import \\iserv\netlogon\login5.reg
  reg import \\iserv\netlogon\local5.reg
:fi
rem Windows Vista and 7
ver | find "Version 6." > nul
if errorlevel 1 goto fi
  reg import \\iserv\netlogon\login6.reg
  reg import \\iserv\netlogon\local6.reg
:fi

rem Windows mounts home share automatically to h:
rem Cannot unmount as h: is current directory
rem net use h: /delete /yes
rem net use h: \\iserv\home /yes
if exist h: goto success
echo Laufwerk H: (Eigene Dateien) nicht verfügbar!
pause
:success

rem Mount group share to g:
net use g: /delete /yes
net use g: \\iserv\groups /yes
if not errorlevel 1 goto success
echo Konnte Laufwerk G: (Gruppen-Dateien) nicht verbinden!
pause
:success

rem Mount windows share to i:
net use i: /delete /yes
net use i: \\iserv\windows /yes
if not errorlevel 1 goto success
echo Konnte Laufwerk I: (Windows-Profil) nicht verbinden!
pause
:success

rem Copy parts of the local user profile to the server to save space (Win2k/XP)
ver | find "Version 5." > nul
if errorlevel 1 goto fi
  if not exist "\\iserv\Windows\Remote\Anwendungsdaten\Microsoft\Internet Explorer\Quick Launch" xcopy /Y /I /E /H /R "%userprofile%\Anwendungsdaten" "\\iserv\Windows\Remote\Anwendungsdaten"
  if not exist "\\iserv\Windows\Remote\Favoriten" xcopy /Y /I /E /H /R "%userprofile%\Favoriten" "\\iserv\Windows\Remote\Favoriten"
:fi

rem print
start wscript \\iserv\netlogon\print.js

rem Exam
if exist \\iserv\exam \\iserv\netlogon\exe\iserv-msg -f \\iserv\netlogon\exam.txt

rem local.bat aufrufen
call \\iserv\netlogon\local.bat

rem Falls vorhanden benutzerdefiniertes Loginscript aufrufen
if exist h:\login.bat call h:\login.bat

