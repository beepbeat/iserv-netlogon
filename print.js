// init begin
var retrydef = 1;
var sleepdef = 0;
var quiet = true;
var errTimeout = 3;
var wbemFlagReturnImmediately = 0x10;
var wbemFlagForwardOnly = 0x20;
var objWMIService = GetObject('winmgmts:\\\\.\\root\\CIMv2');
var wshNetwork = WScript.CreateObject('WScript.Network');
var wshShell = WScript.CreateObject("WScript.Shell")
var fs = new ActiveXObject('Scripting.FileSystemObject');
// init end

// Config begin
var dir = fs.GetFile(WScript.ScriptFullName).ParentFolder;

var config = {server: null, def: false, printers: [], exclude: []};
mergeConfig(dir + '\\Printer Configuration\\default.json');
mergeConfig(dir + '\\Printer Configuration\\default.local.json');

if (!config) {
	throw new Error('Could not load printer configuration.');
}

rooms = readFile(dir + '\\Printer Configuration\\rooms.json', true);
groups = readFile(dir + '\\Printer Configuration\\groups.json', true);

// read ips
var colItems = objWMIService.ExecQuery('SELECT * FROM Win32_NetworkAdapterConfiguration', 'WQL', wbemFlagReturnImmediately | wbemFlagForwardOnly);
var enumItems = new Enumerator(colItems);
var ips = [];
for (; !enumItems.atEnd(); enumItems.moveNext()) {
  var objItem = enumItems.item();
  if (!objItem.IPEnabled) {
    continue;
  }
  ips = ips.concat(objItem.IPAddress.toArray());
}

// room
for (i = 0; i < ips.length; i++) {
  if (rooms['i' + ips[i]]) {
      room = rooms['i' + ips[i]].room.replace('/', '_');
      mergeConfig(dir + '\\Printer Configuration\\room\\' + room + '.json');
      mergeConfig(dir + '\\Printer Configuration\\room.local\\' + room + '.json');
  }
}

// host
for (i = 0; i < ips.length; i++) {
  mergeConfig(dir + '\\Printer Configuration\\host\\' + ips[i] + '.json');
  mergeConfig(dir + '\\Printer Configuration\\host.local\\' + ips[i] + '.json');
}

// group
if (groups[wshNetwork.UserName]) {
  for (i = 0; i < groups[wshNetwork.UserName]['groups'].length; i++) {
    mergeConfig(dir + '\\Printer Configuration\\group\\' + groups[wshNetwork.UserName]['groups'][i] + '.json');
    mergeConfig(dir + '\\Printer Configuration\\group.local\\' + groups[wshNetwork.UserName]['groups'][i] + '.json');
  }
}

// user
mergeConfig(dir + '\\Printer Configuration\\user\\' + wshNetwork.UserName + '.json');
mergeConfig(dir + '\\Printer Configuration\\user.local\\' + wshNetwork.UserName + '.json');

// Process excludes begin
for (i = 0; i < config.exclude.length; i++) {
  for (j = 0; j < config.printers.length; j++) {
    if (config.exclude[i] == config.printers[j]) {
      delete config.printers[j];
    }
  }
}
// Process excludes end

// Check if default printer exists begin
defaultFound = false;
for (i = 0; i < config.printers.length; i++) {
  if (config.printers[i] && config.printers[i] == config.def) {
    defaultFound = true;
  }
}
if (!defaultFound) {
  config.def = false;
}
// Check if default printer exists end

// Config end

// Delete printer connections begin
var oPrinters = wshNetwork.EnumPrinterConnections();
for (i = 1; i < oPrinters.length; i += 2) {
  if (oPrinters.Item(i).indexOf('\\\\' + config.server + '\\') !== 0) {
    continue;
  }
  execute(function(p){wshNetwork.RemovePrinterConnection(p)}, [oPrinters.Item(i)], 'Remove printer', 0, 0);
}

// Clean registry begin
try {
  var key = regGetSubKeys(".", "Printers\\Connections")
  if (key.SubKeys.length) {
    for (var i=0; i < key.SubKeys.length; i++)
    {
      var entry = 'HKCU\\Printers\\Connections\\' + key.SubKeys[i];
      var server = wshShell.RegRead(entry + '\\Server');
      if (server.toLowerCase() == '\\\\' + config.server.toLowerCase()) {
	wshShell.RegDelete(entry + '\\');
      }
    }
  }
} catch (e) {
}
// Clean registry end

// Delete printer connections end

// Add printer connections begin
for (i = 0; i < config.printers.length; i++) {
  if (!config.printers[i]) {
    continue;
  }
  execute(function(s, p){wshNetwork.AddWindowsPrinterConnection('\\\\' + s + '\\' + p)}, [config.server, config.printers[i]], 'Add printer');
}
// Add printer connections end

// Set default printer begin
if (config.def) {
  execute(function(s, p){wshNetwork.SetDefaultPrinter('\\\\' + s + '\\' + p)}, [config.server, config.def], 'Set default printer');
}
// Set default printer end

/*
 * Read a file into a string or, if asObject is true, into an object from JSON.
 */
function readFile(file, asObject) {
  try {
    if (!fs.FileExists(file)) {
      return false;
    }
    f = fs.GetFile(file);
    is = f.OpenAsTextStream(1, 0); // read, ascii

    var content = '';
    if (!is.AtEndOfStream) {
      content = is.ReadAll();
    }
    is.Close();

    if (asObject) {
      content = eval('(' + content + ')');
    }
    return content;
  } catch(e) {
  }
}

/*
 * Merge config objects.
 */
function mergeConfig(file) {
  merge = readFile(file, true);
  if (merge) {
    if (merge.server) {
      config.server = merge.server;
    }
    if (merge.def) {
      config.def = merge.def;
    }
    if (merge.printers) {
      config.printers = config.printers.concat(merge.printers);
    }
    if (merge.exclude) {
      config.exclude = config.exclude.concat(merge.exclude);
    }
  }
}

/*
 * Execute a function and retry until success or canceled by the user.
 */
function execute(f) {
  args = typeof arguments[1] == 'object' ? arguments[1] : undefined;
  action = typeof arguments[2] !== 'undefined' ? arguments[2] : retrydef;
  retryno = typeof arguments[3] !== 'undefined' ? arguments[3] : retrydef;
  sleepno = typeof arguments[4] !== 'undefined' ? arguments[4] : sleepdef;
  tryno = 0;
	
  do {
    var msg = false;
    try {
      f.apply(this, args);
	    return;
    } catch(e) {
	    msg = e.message;
    }
    WScript.Sleep(sleepno);
  } while((tryno++ < retryno) || (!quiet && wshShell.Popup(action + "\n" + args + "\n" + msg, errTimeout, 'Error', 0x5 + 0x10 + 0x1000) == 4));	; // Retry, Cancel + Stop + Modal
}

//-------------------------------------------------------------
// function : regGetSubKeyNames(strComputer, strRegPath)
//
//  purpose : return an array with names of any subKeys
//-------------------------------------------------------------
function regGetSubKeys(strComputer, strRegPath) {
  try
  {
    var HKCU = 0x80000001;
    var aNames = null;
    var objLocator = new ActiveXObject("WbemScripting.SWbemLocator");
    var objService = objLocator.ConnectServer(strComputer, "root\\default");
    var objReg = objService.Get("StdRegProv");
    var objMethod = objReg.Methods_.Item("EnumKey");
    var objInParam = objMethod.InParameters.SpawnInstance_();
    objInParam.hDefKey = HKCU;
    objInParam.sSubKeyName = strRegPath;
    var objOutParam = objReg.ExecMethod_(objMethod.Name, objInParam);
    switch(objOutParam.ReturnValue)
    {
      case 0: // Success
        aNames = (objOutParam.sNames != null) ? objOutParam.sNames.toArray(): null;
        break;

      case 2: // Not Found
        aNames = null;
        break;
    }
    return { Results : 0, SubKeys : aNames };
  }
  catch(e)
  {
    return { Results: e.number, SubKeys : e.description }
  }
}

