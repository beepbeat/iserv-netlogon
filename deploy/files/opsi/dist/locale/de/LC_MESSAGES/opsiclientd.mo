��    =        S   �      8     9     ?     V     r  '   �     �     �     �  $   �  /     +   8  E   d  #   �  -   �  +   �     (     D     \     b     z     �     �     �     �     �     �       
        *     ;     M     _     l       	   �     �      �     �     �     �  F   	     [	     h	     y	     �	     �	  6   �	     �	     
     

     
  	   
     (
     -
     ;
     G
  	   \
     f
  	   l
     v
  a  ~
  	   �     �            ;   2     n  
   �      �  "   �  &   �  3      M   4  /   �  /   �  6   �  "     "   <     _     g     �     �  #   �     �     �               1     C     U     k     �     �     �     �     �     �  (        6  ,   T  ,   �  S   �               #     A  &   `  2   �     �     �     �     �     �     �     �     	  %        A     R     Y     h     2   <                   .   (      6      +      !                   9           4   3          *              
      8   ,                          :   5                 )       	       0         1                 ;      7         '   #             =   "   $       %   &   -       /                  Abort Action processor ended Action processor is running Actions completed Actions will be processed on next boot. Caching products Close Connected to config server '%s' Connecting to config server '%s' #%d Event %s: action processing will start in %s:%s Failed to connect to config server '%s': %s Failed to connect to config server '%s': Service verification failure Failed to process login actions: %s Failed to process product action requests: %s Getting action requests from config service Getting config from service Got config from service Later Mounting depot share %s No product action requests set Nothing selected Other pending product actions: Processing event %s Processing login actions Products Products cached Reboot in %s:%s Reboot now Reboot requested Rebooting machine Shutdown in %s:%s Shutdown now Shutdown requested Shutting down machine Start now Starting actions Starting to process actions now. Sync completed Syncing config from server Syncing config to server The following product actions have been added to fulfill dependencies: Timeout: %ds Unblocking login Updating action processor Waiting for TrustedInstaller Writing log to service You selected to execute the following product actions: advice back description install installed next not installed process now process on next boot reinstall state uninstall version Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2011-08-31 11:40+CEST
PO-Revision-Date: 2010-09-08 16:59+CEST
Last-Translator: Automatically generated
Language-Team: none
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 Abbrechen Action processor beendet Action processor läuft Aktionen abgeschlossen Die Aktionen werden beim nächsten Systemstart ausgeführt. Speichere Produkte zwischen Schliessen Verbunden mit Config-Server '%s' Verbinde zu Config-Server '%s' #%d Ereignis %s: Aktionen starten in %s:%s Verbindung zu Config-Server '%s' fehlgeschlagen: %s Verbindung zu Config-Server '%s' fehlgeschlagen: Server-Verifizierungs-Fehler Fehler beim Ausführen der Anmelde-Aktionen: %s Fehler beim Ausführen der Produkt-Aktionen: %s Frage auszuführende Aktionen beim Config-Service nach Frage Konfiguration auf Service ab Konfiguration von Service erhalten Später Verbinde mit Depot-Freigabe %s Keine Produkt-Aktionen gesetzt Nichts ausgewählt Andere anstehende Produkt-Aktionen: Verarbeite Ereignis %s Starte Anmelde-Aktionen Produkte Produkte zwischengespeichert Neustart in %s:%s Jetzt neu starten Neustart erforderlich Computer wird neu gestartet Herunterfahren in %s:%s Jetzt herunterfahren Herunterfahren erforderlich Computer wird heruntergefahren Jetzt starten Starte Aktionen Beginne mit dem Ausführen der Aktionen. Synchronisation abgeschlossen Synchronisation der Konfiguration vom Server Synchronisation der Konfiguration zum Server Die folgenden Produkt-Aktionen wurden hinzugefügt um Abhängigkeiten zu erfüllen: Timeout: %ds Gebe Anmeldung frei Aktualisiere action processor Warte auf den TrustedInstaller Übertrage Protokoll-Datei zum Service Die folgenden Produkt-Aktionen wurden ausgewählt: Hinweis zurück Beschreibung installieren installiert weiter nicht installiert sofort ausführen beim nächsten Systemstart ausführen neu installieren Status deinstallieren Version 