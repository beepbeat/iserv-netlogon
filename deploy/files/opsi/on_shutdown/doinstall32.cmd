@echo off
rem *****************************************************************
rem opsiclientd_shutdown_starter.exe performs the following actions:
rem - read credentials from opsiclientd.conf
rem - connect to opsiclientd
rem - read backend info for modules file 
rem   (module on_shutdown is activated?)
rem - fire event on_shutdown (default=gui_startup) 
rem   to start product Installation on client
rem - wait for completion of installation (end of task)
rem *****************************************************************
echo Start opsi product installation ...
echo doinstall32.cmd started>C:\opsi.org\tmp\doinstall.log
type enter.txt|date>>C:\opsi.org\tmp\doinstall.log
cd "%ProgramFiles%\opsi.org\opsi-client-agent">>C:\opsi.org\tmp\doinstall.log
opsiclientd_shutdown_starter.exe on_shutdown>>C:\opsi.org\tmp\doinstall.log

