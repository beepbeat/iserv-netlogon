#! /bin/bash 

set -x

#if %1!==/u! goto startwork
#if %2!==/u! goto startwork
echo .
echo   Aktueller PC: $HOSTNAME
echo   You are at PC: $HOSTNAME
echo .
echo   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo   !!                                                                    !!
echo   !!           Zum Starten der opsi-client-agent Installation           !!
echo   !!             druecken Sie bitte eine beliebige Taste                !!
echo   !!        Zum Abbrechen schliessen Sie einfach dieses Fenster         !!
echo   !!                                                                    !!
echo   !!          To start the installation of the opsi-client-agent        !!
echo   !!                        just press any key                          !!
echo   !!                 To cancel just close this window                   !!
echo   !!                                                                    !!
echo   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo .
read


mkdir -p /tmp/opsi
mkdir -p /var/log/opsi-client-agent
cp -r files/opsi/* /tmp/opsi
chmod u+x "/tmp/opsi/opsi-script/opsi-script"
export DISPLAY=:0
sudo "/tmp/opsi/opsi-script/32/opsi-script" -batch /tmp/opsi/setup.ins /var/log/opsi-client-agent/opsi-script/opsi-client-agent.log -PARAMETER INSTALL:CREATE_CLIENT:NOREBOOT

echo .
echo   Installation abgeschlossen
echo   Installation completed
echo .
read

