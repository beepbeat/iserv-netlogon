@echo off
rem usage: install.bat pckey opsiserver

rem The client agent is 32 bit and will therefore only look in the 32 bit
rem Program Files folder. We determine the correct folder here:
if defined ProgramFiles(x86) (
  set "prog=%ProgramFiles(x86)%"
) else (
  set prog=%ProgramFiles%
)

set pckey=%1
set opsiserver=%2
set confdir=%prog%\opsi.org\opsi-client-agent\opsiclientd
set conf=%confdir%\opsiclientd.conf
set opsi=%~dp0files\opsi

if not exist "%confdir%" mkdir "%confdir%"
echo [global] > "%conf%"
echo opsi_host_key=%pckey% >> "%conf%"
echo [config_service] >> "%conf%"
echo url=https://%opsiserver%:4447 >> "%conf%"

"%opsi%\opsi-winst\winst32.exe" /batch "%opsi%\setup.ins" "C:\tmp\opsi-client-agent.log"

